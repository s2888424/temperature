package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static nl.utwente.di.temperature.Converter.convert;

/*** Test the temperature */
public class TestTemperature {
    private Converter converter;
    @Test
    public void testBook1() throws Exception{
        double temperature = convert("37");
        Assertions.assertEquals(98.6,temperature, 0.0,"temperature");
    }
}